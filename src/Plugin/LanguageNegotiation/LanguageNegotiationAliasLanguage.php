<?php

namespace Drupal\alias_language_negotiation\Plugin\LanguageNegotiation;

use Drupal\Core\Path\AliasStorageInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Url;
use Drupal\Core\Language\LanguageInterface;
use Drupal\language\LanguageSwitcherInterface;
use Drupal\language\Plugin\LanguageNegotiation\LanguageNegotiationContentEntity;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class for identifying the content translation language.
 *
 * @LanguageNegotiation(
 *   id = Drupal\alias_language_negotiation\Plugin\LanguageNegotiation\LanguageNegotiationAliasLanguage::METHOD_ID,
 *   types = {Drupal\Core\Language\LanguageInterface::TYPE_CONTENT},
 *   weight = -9,
 *   name = @Translation("Alias language"),
 *   description = @Translation("Determines the content language from the alias language."),
 * )
 */
class LanguageNegotiationAliasLanguage extends LanguageNegotiationContentEntity implements InboundPathProcessorInterface, LanguageSwitcherInterface {

  /**
   * The language negotiation method ID.
   */
  const METHOD_ID = 'language-alias-language';

  /**
   * The alias storage.
   *
   * @var \Drupal\Core\Path\AliasStorageInterface
   */
  protected $aliasStorage;

  /**
   * Constructs a new LanguageNegotiationAliasLanguage instance.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\Core\Path\AliasStorageInterface $alias_storage
   *   The alias storage.
   */
  public function __construct(EntityManagerInterface $entity_manager, AliasStorageInterface $alias_storage) {
    parent::__construct($entity_manager, new \SplObjectStorage());
    $this->aliasStorage = $alias_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('entity.manager'),
      $container->get('path.alias_storage')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getLangcode(Request $request = NULL) {
    $alias = $this->loadAlias($request->getPathInfo());
    if (!$alias) {
      return NULL;
    }

    $langcode = $alias['langcode'];
    $language_enabled = array_key_exists($langcode, $this->languageManager->getLanguages());
    return $language_enabled ? $langcode : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    $alias = $this->loadAlias($request->getPathInfo());
    if (!$alias) {
      return $path;
    }

    return $alias['source'];
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    // If appropriate, process outbound to add a query parameter to the url
    // and remove the language option, so that the url negotiator does not
    // rewrite the url.
    //
    // First, check if processing conditions are met.
    //
    // NOTE: recommends https://www.drupal.org/node/2863465
    if (!($request && !empty($options['route']) && $this->hasLowerLanguageNegotiationWeight() && $this->meetsContentEntityRoutesCondition($options['route'], $request))) {
      return $path;
    }

    if (isset($options['language'])) {
      // If the url comes from an interface language switcher, leave
      // $options['language'] alone so that the interface language can be
      // changed, but keep the current request path to still use the path
      // alias if there is one.
      //
      // NOTE: depends on https://www.drupal.org/node/2864055
      if (isset($options['language_switch_link_type']) && $options['language_switch_link_type'] == LanguageInterface::TYPE_INTERFACE) {
        return $this->stripPathPrefix($request->getPathInfo());
      }
      // Otherwise, unset $options['language'] in order to preserve the
      // current prefix in the url.
      elseif (isset($options['language'])) {
        unset($options['language']);
      }
    }

    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function getLanguageSwitchLinks(Request $request, $type, Url $url) {
    $links = [];

    $internal_path = '/' . $url->getInternalPath();
    $prefix = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_URL)->getId();

    foreach ($this->languageManager->getNativeLanguages() as $language) {
      $langcode = $language->getId();
      $alias = $this->aliasStorage->lookupPathAlias($internal_path, $langcode);
      if ($alias) {
        // Since these links are meant to switch the _content_ language, use
        // the path alias for the destination language, but keep the current
        // interface language prefix.
        $new_url = Url::fromUri('base:/' . $prefix . $alias);
      }
      else {
        // NOTE: An alternative would be to skip this part antirely and don't
        // show a link at all if there is no alias for the translation.  or
        // even set the route to 'system.404'.
        $new_url = clone $url;
        $new_url->setOption('language', NULL);
      }

      $links[$langcode] = [
        'url' => $new_url,
        'title' => $language->getName(),
        'attributes' => ['class' => ['language-link']],
      ];
    }

    return $links;
  }

  /**
   * Helper function to get an alias from a prefixed path.
   *
   * @param string $path_alias
   *   A resource path.
   *
   * @return array
   *   The alias array corresponding to $path_alias or FALSE.
   *   See AliasStorage::load()
   */
  protected function loadAlias($path_alias) {
    $unprefixed_path = $this->stripPathPrefix($path_alias);

    $conditions = ['alias' => $unprefixed_path];

    return $this->aliasStorage->load($conditions);
  }

  /**
   * Helper function to strip the language prefix from multilingual paths.
   *
   * @param string $path_info
   *   Path that might contain a language prefix.
   *
   * @return string
   *   Path without the language prefix.
   */
  protected function stripPathPrefix($path_info) {
    $parts = explode('/', trim($path_info, '/'));
    $prefix = array_shift($parts);
    if ($prefix == $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_URL)->getId()) {
      return '/' . implode('/', $parts);
    }

    return $path_info;
  }

}
